<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function beranda ()
    {
        return view ('beranda');
    }
    public function show ()
    {
        $karyawan = DB::table('Karyawan')->get();

        return view ('data_karyawan.data',['k'=>$karyawan]);
    }
    public function add ()
    {
        return view ('data_karyawan.add');
    }

    public function prosesSimpan (Request $req)
    {
      
        DB::table('Karyawan')->insert(
            ['nama_karyawan'=>$req->nama,
             'no_karyawan'=>$req->no,
             'no_tlp_karyawan'=>$req->notlp,
             'jabatan_karyawan'=>$req->jabatan,
             'divisi_karyawan'=>$req->divisi,
            ]);

       
        return redirect('/datas')->with('status', 'Data Berhasil Ditambah!');
    }

    public function edit($id)
    {
        $datas = DB::table('Karyawan')->where('id', $id)->first();
      
        return view ('data_karyawan.edit', compact('datas'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('Karyawan')->where('id', $id)
                    ->update([
                        'nama_karyawan'=>$req->nama,
                        'no_karyawan'=>$req->no,
                        'no_tlp_karyawan'=>$req->notlp,
                        'jabatan_karyawan'=>$req->jabatan,
                        'divisi_karyawan'=>$req->divisi,
                    ]);
      
        return redirect('/datas')->with('status', 'Data Berhasil Diupdate!');
    }

    public function delete($id)
    {
       DB::table('Karyawan')->where('id',$id )->delete();
       return redirect('/datas')->with('status', 'Data Berhasil Dihapus!');
    }
} 
