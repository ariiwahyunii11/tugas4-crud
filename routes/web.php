<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('beranda', function () {
//     return view('beranda');
// });
// // Route::get('add', function () {
// //     return view('data_karyawan.add');
// // });
Route::get('/', [DataController::class, 'beranda'])->name('beranda');
Route::get('datas', [DataController::class, 'show'])->name('datas');
Route::get('add', [DataController::class, 'add'])->name('add');
Route::post('datas', [DataController::class, 'prosesSimpan'])->name('datas');
Route::get('datas/edit/{id}', [DataController::class, 'edit'])->name('datas/edit/{id}');
Route::put('datas/edit/{id}', [DataController::class, 'prosesEdit'])->name('datas/edit/{id}');
Route::delete('datas/{id}', [DataController::class, 'delete'])->name('datas/{id}');