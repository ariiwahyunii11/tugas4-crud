@extends('layout.main')
@section('title','Data')
@section('breadcrumbs','MyKaryawan')
@section('navKaryawan','active')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>  
        @endif
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Data Karyawan</strong>
                    </div>
                    <div class="pull-right">
                        <a href="/add" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i> Add Data
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>No.</th>
                                <th>Nama Karyawan</th>
                                <th>No Karyawan</th>
                                <th>No Tlp Karyawan</th>
                                <th>Jabatan Karyawan</th>
                                <th>Divisi Karyawan</th> 
                                <th>Action</th>   
                            </tr>
                            
                            <tbody>
                                @foreach ($k as $item)
                                    <tr>
                                    <td>{{ $loop->iteration }}</td>  
                                    <td>{{ $item->nama_karyawan }}</td>
                                    <td>{{ $item->no_karyawan }}</td>
                                    <td>{{ $item->no_tlp_karyawan }}</td>
                                    <td>{{ $item->jabatan_karyawan }}</td>
                                    <td>{{ $item->divisi_karyawan }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('datas/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{ url('datas/'.$item->id) }}" method="post" class="d-inline" onsubmit="return confirm('Yakin Hapus Data?')">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection